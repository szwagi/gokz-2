"Phrases"
{
	// =====[ GENERAL ]=====
	"Offset"
	{
		"en"		"Offset"
		"chi"		"位移"
	}
	"Offset Short"
	{
		"en"		"Off"
	}
	"Height"
	{
		"en"		"Height"
		"chi"		"高度"
	}
	"Pre"
	{
		"en"		"Pre"
		"chi"		"Pre"
	}
	"Max"
	{
		"en"		"Max"
		"chi"		"Max"
	}
	"Strafes" // Plural
	{
		"en"		"Strafes"
		"chi"		"加速"
	}
	"Strafe" // Singular
	{
		"en"		"Strafe"
		"chi"		"加速"
	}
	"Sync"
	{
		"en"		"Sync"
		"chi"		"同步率"
	}
	"Airtime"
	{
		"en"		"Airtime"
		"chi"		"滞空"
	}
	"units"
	{
		"en"		"units"
		"chi"		"单位"
	}
	"Avg. Width"
	{
		"en"		"Avg. Width"
	}
	"Block"
	{
		"en"		"Block"
	}
	"Overlap"
	{
		"en"		"OL"
	}
	"Dead Air"
	{
		"en"		"DA"
	}
	"Edge"
	{
		"en"		"Edge"
	}
	"Edge Short"
	{
		"en"		"E"
	}
	"Deviation"
	{
		"en"		"Deviation"
	}
	"W Release"
	{
		"en"		"W"
	}
	"Width"
	{
		"en"		"Width"
	}
	"Speed"
	{
		"en"		"Speed"
	}
	"Crouch Ticks"
	{
		"en"		"Crouched"
	}
	"Crouch Release"
	{
		"en"		"C"
	}
	"Miss"
	{
		"en"		"Miss"
	}


	// =====[ CHAT MESSAGES ]=====
	"Dropped Too Far (Weird Jump)"
	{
		// You fell too for for a valid Weird Jump (96.0/64.0).
		"#format"	"{1:.1f},{2:.1f}"
		"en"		"{grey}You fell too far for a valid Weird Jump ({lightred}{1}{grey}/{green}{2}{grey})."
		"chi"		"{grey}你的下落距离太高,无法判定为WJ (实际距离:{lightred}{1}{grey}/高度上限:{green}{2}{grey})."
	}
	"Jumpstats Option - Master Switch - Enable"
	{
		"en"		"{grey}You have enabled jumpstats reporting."
		"chi"		"{grey}已开启起跳数据."
	}
	"Jumpstats Option - Master Switch - Disable"
	{
		"en"		"{grey}You have disabled all jumpstats reporting."
		"chi"		"{grey}已关闭起跳数据."
	}
	"Jumpstats Option - Jumpstats Always - Enable"
	{
		"en"		"{grey}You have enabled the always-on jumpstats."
	}
	"Jumpstats Option - Jumpstats Always - Disable"
	{
		"en"		"{grey}You have disabled the always-on jumpstats."
	}
	"Broadcast Jumpstat Chat Report"
	{
		"#format"	"{1:s},{2:N},{3:.2f},{4:s}"
		"en"		"{1}{2} jumped {3} units with a {4}"
	}


	// =====[ CONSOLE MESSAGES ]===== 
	"Console Jump Header"
	{
		// Bill jumped 268.3485 units with a Long Jump
		"#format"	"{1:N},{2:.4f},{3:s}"
		"en"		"{1} jumped {2} units with a {3}"
		"chi"		"{1} 以{3}方式跳出了{2} 单位"
	}
	"Console Failstat Header"
	{
		// Bill failed a 268.3485 unit Long Jump
		"#format"	"{1:N},{2:.4f},{3:s}"
		"en"		"{1} failed a {2} unit {3}"
	}
	// NOTE: For table formatting in console, these 'headers' should
	// be the same number of characters as the English translation.
	"Sync (Table)"
	{
		"en"		"Sync      "
		"chi"		"同步      "
	}
	"Gain (Table)"
	{
		"en"		"Gain      "
		"chi"		"获得      "
	}
	"Loss (Table)"
	{
		"en"		"Loss      "
		"chi"		"损失      "
	}
	"Airtime (Table)"
	{
		"en"		"Airtime   "
		"chi"		"滞空时间  "
	}
	"Width (Table)"
	{
		"en"		"Width     "
	}
	"Overlap (Table)"
	{
		"en"		"OL     "
	}
	"Dead Air (Table)"
	{
		"en"		"DA     "
	}


	// =====[ OPTIONS MENU ]=====
	"Options Menu - Jumpstats"
	{
		"en"		"Jumpstats"
	}
	"Options Menu - Jumpstats Master Switch"
	{
		"en"		"Jumpstats"
		"chi"		"起跳数据显示"
	}
	"Options Menu - Jumpstats Chat Report"
	{
		"en"		"Chat report"
		"chi"		"聊天框显示"
	}
	"Options Menu - Jumpstats Console Report"
	{
		"en"		"Console report"
		"chi"		"控制台输出显示"
	}
	"Options Menu - Jumpstats Sounds"
	{
		"en"		"Sounds"
		"chi"		"音效"
	}
	"Options Menu - Extended Jump Chat Report"
	{
		"en"		"Extended Chat Report"
	}
	"Options Menu - Failstats Console Report"
	{
		"en"		"Failstats Console Report"
	}
	"Options Menu - Failstats Chat Report"
	{
		"en"		"Failstats Chat Report"
	}
	"Options Menu - Jumpstats Always"
	{
		"en"		"Always Print Jumpstats"
	}
	"Options Menu - Minimal Jump Chat Broadcast Tier"
	{
		"en"		"Min Chat Broadcast Tier"
	}
	"Options Menu - Minimal Jump Sound Broadcast Tier"
	{
		"en"		"Min Sound Broadcast Tier"
	}
}
